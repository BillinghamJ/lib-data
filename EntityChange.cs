namespace Tam.Lib.Data
{
  /// <summary>
  /// Makes changes to entity as required
  /// </summary>
  /// <typeparam name="T">Entity type</typeparam>
  /// <param name="e">Referenced entity to change</param>
  public delegate void EntityChange<T>(ref T e);
}
