using System.Collections.Generic;
using Tam.Lib.Model;

namespace Tam.Lib.Data
{
  /// <summary>
  /// Generic entity repository
  /// </summary>
  /// <typeparam name="T">Entity type</typeparam>
  public interface IRepository<T> : IEnumerable<T> where T : class, IEntity
  {
    /// <summary>
    /// Gets entire set of entities
    /// </summary>
    /// <returns>All entities</returns>
    IEnumerable<T> GetAll();

    /// <summary>
    /// Gets an entity with a certain ID
    /// </summary>
    /// <param name="id">Entity identifier</param>
    /// <returns>Entity with ID</returns>
    T Get(long id);

    /// <summary>
    /// Creates and stores a new entity
    /// </summary>
    /// <param name="entity">Entity to create</param>
    /// <returns>Stored created entity</returns>
    T Create(T entity);

    /// <summary>
    /// Changes and stores an entity
    /// </summary>
    /// <param name="entity">Entity to change</param>
    /// <param name="change">Changes to make</param>
    /// <returns>Stored changed entity</returns>
    T Change(T entity, EntityChange<T> change);

    /// <summary>
    /// Deletes an entity
    /// </summary>
    /// <param name="entity">Entity to delete</param>
    void Delete(T entity);
  }
}
