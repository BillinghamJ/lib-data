using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Tam.Lib.Model;

namespace Tam.Lib.Data
{
  public abstract class ARepository<T> : IRepository<T>, IEnumerable<T> where T : class, IEntity
  {
    protected IDbContext Database { get; set; }
    protected IDbSet<T> Data { get; set; }

    protected ARepository(IDbContext database, IDbSet<T> data)
    {
      Database = database;
      Data = data;
    }

    #region Get

    public IEnumerable<T> GetAll()
    {
      return Data;
    }

    public T Get(long id)
    {
      return Data.SingleOrDefault(e => e.Id == id);
    }

    #endregion

    #region Create

    public T Create(T entity)
    {
      ValidateEntity(entity);
      ValidateCreation(entity);

      Data.Add(entity);
      Database.SaveChanges();

      return entity;
    }

    #endregion

    #region Change

    public T Change(T entity, EntityChange<T> change)
    {
      var before = entity;

      change(ref entity);

      ValidateEntity(entity);
      ValidateChange(before, entity);

      Database.SaveChanges();

      return entity;
    }

    #endregion

    #region Delete

    public void Delete(T entity)
    {
      ValidateDeletion(entity);

      Data.Remove(entity);
      Database.SaveChanges();
    }

    #endregion

    #region Validation

    protected virtual void ValidateEntity(T entity) { }
    protected virtual void ValidateCreation(T entity) { }
    protected virtual void ValidateChange(T before, T after) { }
    protected virtual void ValidateDeletion(T entity) { }

    #endregion

		#region IEnumerable

		public IEnumerator<T> GetEnumerator()
	  {
		  return Data.GetEnumerator();
	  }

	  IEnumerator IEnumerable.GetEnumerator()
	  {
		  return GetEnumerator();
		}

		#endregion
	}
}
